public class No {

    private int p;
    private No prox;

    public int getP() {
        return p;
    }
    public No(int pe){
        this.p = pe;
        this.prox = null;
    }

    public void setP(int p) {
        this.p = p;
    }

    public No getProx() {
        return prox;
    }

    public void setProx(No prox) {
        this.prox = prox;
    }

    @Override
    public String toString() {
        return "No{" +
                "p=" + p +
                ", prox=" + prox +
                '}';
    }
}
