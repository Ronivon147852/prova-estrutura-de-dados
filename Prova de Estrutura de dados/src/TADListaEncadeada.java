public interface TADListaEncadeada {

    ILista[] dividirLista(ILista original, int posicao) throws Exception;
    ILista ordenarnarDecrescente(int[] ordenada);


    void inserirPrimeiro(int p);
    void inserirNoMeio(int indice, int p);
    boolean removerNo(int elemento);
    int pesquisarNo(int elemento);
    String imprimirLista();
    boolean eVazia();
}
