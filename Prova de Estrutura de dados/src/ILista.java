public class ILista implements TADListaEncadeada{
    private No prim;
    private No ult;
    private int quantiNo;
    private ILista teste;
    private int parada = 0;

    public ILista(){
        this.prim = null;
        this.ult = null;
        this.quantiNo = 0;
    }

    public No getPrim() {
        return prim;
    }

    public void setPrim(No prim) {
        this.prim = prim;
    }

    public No getUlt() {
        return ult;
    }

    public void setUlt(No ult) {
        this.ult = ult;
    }

    public int getQuantiNo() {
        return quantiNo;
    }

    public void setQuantiNo(int quantiNo) {
        this.quantiNo = quantiNo;
    }


    // -------------METODOS DA PROVA------------------------
    @Override
    public ILista[] dividirLista(ILista original, int posicao) throws Exception {
        No atual = prim;
        ILista ladoEsquerdoList = new ILista();
        ILista ladoDireitoList = new ILista();

        ILista[] results = new ILista[quantiNo];

        if (posicao < 0 || posicao > quantiNo){
            System.out.println("Indice fora dos parâmetros!");
        }else {
                for (int i = 0; i <= quantiNo; i++) {
                    if (i <= posicao) {
                        ladoEsquerdoList.inserirUltimo(atual.getP());
                        atual = atual.getProx();
                    } else {
                        ladoDireitoList.inserirUltimo(atual.getP());
                        atual = atual.getProx();
                    }
                }
                results[0] = ladoEsquerdoList;
                results[1] = ladoDireitoList;
            }
        return results;
    }

    public ILista ordenarnarDecrescente(int[] ordenada){
        int aux = (ordenada.length-1) - parada;
        if (parada == 0)
            criarLista();
        if (parada == ordenada.length) {
            return teste;
        }
        teste.inserirUltimo(ordenada[aux]);
        parada++;
        return ordenarnarDecrescente(ordenada);
    }
    // -------------FIM METODOS DA PRAVA---------------------

    private ILista criarLista(){
        teste = new ILista();
        return teste;
    }




    public void inserirPrimeiro(int p){
        No novoNo = new No(p);
        if (eVazia()){
            this.ult = novoNo;
        }
        novoNo.setProx(this.prim);
        this.prim = novoNo;
        this.quantiNo++;
    }
    public void inserirUltimo(int p){
        No novoNo = new No(p);
        if (eVazia()){
            this.prim = novoNo;
        }
        else{
            this.ult.setProx(novoNo);
        }
        this.ult = novoNo;
        this.quantiNo ++;
    }
    public void inserirNoMeio(int indice, int p){
        No novoNo = new No(p);
        No atual = this.prim;
        No ant = null;

        if (eVazia()){
            this.prim = novoNo;
        }
        else{
            if (indice < 0 || indice >= quantiNo){
                System.out.println("Indice fora dos parâmetros!");
            }else {

                if (indice == 0) {
                    novoNo.setProx(this.prim);
                    this.prim = novoNo;
                } else {
                    for (int i = 0; i < quantiNo; i++) {
                        if (i != indice) {
                            ant = atual;
                            atual = atual.getProx();
                        } else {
                            ant.setProx(novoNo);
                            novoNo.setProx(atual);
                            quantiNo++;
                            break;
                        }
                    }
                }
            }
        }
    }
    public boolean removerNo(int elemento){
        No atual = this.prim;
        No ant = null;
        if (eVazia()){
            return false;
        }else{
            while(atual != null && !atual.equals(elemento)){
                ant = atual;
                atual = atual.getProx();
            }
            if (atual == this.prim){
                if (this.prim == this.ult){
                    this.ult = null;
                }
                this.prim = this.prim.getProx();
            }else{
                if(atual == this.ult){
                    this.ult = ant;
                }
                ant.setProx(atual.getProx());
            }
            this.quantiNo--;
            return true;
        }
    }
    public int pesquisarNo(int elemento){
        No atual = this.prim;
        while((atual != null) && (!atual.equals(elemento))){
            atual = atual.getProx();
        }
        if (atual != null){
           return elemento;
        }
        return -1;
    }
    public String imprimirLista(){
        String msg="";
        if (eVazia()){
            msg = "A Lista está vazia!";
        }else{
            No atual = this.prim;
            while(atual != null){
                msg += atual.getP() + " -> ";
                atual = atual.getProx();
            }
        }
        return msg;
    }
    public boolean eVazia(){
        return (this.prim == null);
    }

    @Override
    public String toString() {
        return "ListaSimples{" +
                "prim=" + prim +
                ", ult=" + ult +
                ", quantiNo=" + quantiNo +
                '}';
    }

}
